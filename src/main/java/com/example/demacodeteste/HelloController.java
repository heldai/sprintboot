package com.example.springboot;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.client.RestTemplate;
import org.springframework.http.ResponseEntity;
import com.google.gson.Gson; 
import com.google.gson.JsonObject; 
import org.json.JSONObject;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray; 
import java.net.URI;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpEntity;
import java.awt.PageAttributes.MediaType;
import java.io.*;
import java.lang.reflect.Type;
import com.google.gson.reflect.TypeToken;
import java.util.ArrayList;
import com.fasterxml.jackson.core.JsonProcessingException;
import java.util.List;



@RestController
public class HelloController {

	@RequestMapping("/")
	public String index() {
		return "Teste Demacode!";
	}


	@RequestMapping("/getForObjectOperation")
	public String teste(){
		Pedido [] arrayPedidos;
		PedidoEnvio [] Pedidos;
		String result;
		try {

			RestTemplate restTemplate = new RestTemplate();

			//array que recebe os dados e já lança os dados para as classes do model
			arrayPedidos = restTemplate.getForObject("http://origem.demacode.com.br:8181/WS/Pedido?data_inicial=2020-07-07T01:01&data_final=2020-01-01T01:01", Pedido[].class);
			Pedidos = new PedidoEnvio[arrayPedidos.length];	
			for( int i=0; i < arrayPedidos.length; i++){

				// Atribuindo na classe PedidoEnvio apenas os valores que precisam ser postados no webservice destino
				PedidoEnvio p = new PedidoEnvio();
				p.setCpfCnpjCliente(arrayPedidos[i].cliente.cpfCnpj);
				p.setCpfCnpjRepresentante(arrayPedidos[i].representante.cpfCnpj);
				p.setEnderecoEntrega(arrayPedidos[i].cliente.endereco);
				p.setNumero(arrayPedidos[i].numero);
				p.setValorDesconto(arrayPedidos[i].valorDesconto);
				p.setValorFrete(arrayPedidos[i].valorFrete);
				p.setProdutos(arrayPedidos[i].itens);

				float total = 0.00f;
					for(int j=0; j < arrayPedidos[i].itens.length; j++ ){
						//Total dos pedidos vai sendo somado
						total = total + arrayPedidos[i].itens[j].valorTotal;
					}
				//valor total do pedidoEnvio será a soma dos valores dos itens
				p.setValorTotal(total);
				Pedidos[i] = p;
			}

			result = postForObjectOperation(Pedidos);
			/*
			List<PedidoEnvio> listaPedidos = new ArrayList<>();
			Type type = new TypeToken<List<PedidoEnvio>>(){}.getType();
			//transformando em JSon
			Gson gson = new Gson(); 
			JSONOBject pedidosJson = new JSONObject();   
			pedidosJson = gson.toJson(listaPedidos, type);
			headers = new HttpHeaders();
			headers.setContentType(MediaType.APLICATION_JSON);
			String url = "http://destino.demacode.com.br:8282/v1/pedido";
			HttpEntity<String> request = new HttpEntity<String>(pedidosJsonObject.toString(), headers);
			String pedidoPost = restTemplate.postForObject(url, request, String.class);
			*/



		} catch (Exception e) {
    		return e.toString();
		}

		return "GET e POST realizados ;)";
	}

	public String postForObjectOperation ( @RequestBody PedidoEnvio[] inputRequest )  { 
		RestTemplate restTemplate = new RestTemplate();
        try {
		    final PedidoEnvio responseBody = restTemplate.postForObject( "http://destino.demacode.com.br:8282/v1/pedido", inputRequest, PedidoEnvio.class ) ; 
        } catch (Exception e) {
            return e.toString();
        }
		return "OK"; 
	}
	

}

	
