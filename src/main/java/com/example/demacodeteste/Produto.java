package com.example.springboot;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import javax.xml.bind.annotation.XmlRootElement;

public class Produto{

@JsonIgnoreProperties(ignoreUnknown = true)
float precoUnitario;
float quantidade;
String sku;
float valorTotal;
}
