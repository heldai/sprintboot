package com.example.springboot;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import javax.xml.bind.annotation.XmlRootElement;

public class Endereco_Envio{
   
@JsonIgnoreProperties(ignoreUnknown = true)
String bairro;	
String cidade;
String complemento;	
String logradouro;	
String numero;	
String pais;	
String uf;
}
