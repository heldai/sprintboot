package com.example.springboot;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import javax.xml.bind.annotation.XmlRootElement;

public class PedidoEnvio{
   
@JsonIgnoreProperties(ignoreUnknown = true)
String cpfCnpjCliente;
String cpfCnpjRepresentante;
Endereco enderecoEntrega;
String numero;
Item[] produtos;
float valorDesconto;
float valorFrete;
float valorTotal;

	//getters and setters

	public Item[] getProdutos() {
		return this.produtos;
	}

	public void setProdutos(Item[] produtos) {
		this.produtos = produtos;
	}

	public String getCpfCnpjCliente() {
		return this.cpfCnpjCliente;
	}

	public void setCpfCnpjCliente(String cpfCnpjCliente) {
		this.cpfCnpjCliente = cpfCnpjCliente;
	}

	public String getCpfCnpjRepresentante() {
		return this.cpfCnpjRepresentante;
	}

	public void setCpfCnpjRepresentante(String cpfCnpjRepresentante) {
		this.cpfCnpjRepresentante = cpfCnpjRepresentante;
	}

	public Endereco getEnderecoEntrega() {
		return this.enderecoEntrega;
	}

	public void setEnderecoEntrega(Endereco enderecoEntrega) {
		this.enderecoEntrega = enderecoEntrega;
	}

	public String getNumero() {
		return this.numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public float getValorDesconto() {
		return this.valorDesconto;
	}

	public void setValorDesconto(float valorDesconto) {
		this.valorDesconto = valorDesconto;
	}

	public float getValorFrete() {
		return this.valorFrete;
	}

	public void setValorFrete(float valorFrete) {
		this.valorFrete = valorFrete;
	}

	public float getValorTotal() {
		return this.valorTotal;
	}

	public void setValorTotal(float valorTotal) {
		this.valorTotal = valorTotal;
	}

}