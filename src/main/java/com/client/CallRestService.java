package com.example.springboot;
import org.springframework.stereotype.Component;
import java.util.Timer;
import java.util.TimerTask;

@Component
//classe para consumir informações do WebService Origem
public class CallRestService{
    // executando a cada 5 minutos
    final long TEMPO = (5000 * 60); 
    Timer timer = new Timer();
 /*   TimerTask timerTask = new TimerTask() {
        try {
        // recebimento dos dados
        Rest Template restTemplate = new RestTemplate();
        Cliente cliente  = restTemplate.getForObject('http://origem.demacode.com.br:8181/WS/Pedido?data_inicial=2020-01-01T01:01&data_final=2020-01-01T01:01', Person.class);
        
            //validação de algumas informações
            if( (validaCPF(Cliente cliente.getEmail())) && (validaEmail(Cliente cliente.getEmail())) ){

            }throw new Exception ("Dados inválidos");
           
        } catch (Exception e) {
        //TODO: handle exception
        }
    };
    timer.scheduleAtFixedRate(timerTask, TEMPO, TEMPO);
*/
    
    //funções de validações
    public static boolean validaEmail(Cliente cli) {
        boolean result = true;
/*        try {
            InternetAddress emailAddr = new InternetAddress(cli.getEmail());
            emailAddr.validate();
        } catch (AddressException ex) {
            result = false;
        }*/
        return result;
    }


    public static boolean validaCPF(Cliente cli) {  
        boolean result = true;    
        if (    (cli.getCpfCnpj().equals("00000000000")) || (cli.getCpfCnpj().equals("11111111111")) ||
                (cli.getCpfCnpj().equals("22222222222")) || (cli.getCpfCnpj().equals("33333333333")) ||
                (cli.getCpfCnpj().equals("44444444444")) || (cli.getCpfCnpj().equals("55555555555")) ||
                (cli.getCpfCnpj().equals("66666666666")) || (cli.getCpfCnpj().equals("77777777777")) ||
                (cli.getCpfCnpj().equals("88888888888")) || (cli.getCpfCnpj().equals("99999999999")) ||
                (cli.getCpfCnpj().length()!= 11)) 
        {
                    result = false;
        }

        return result;
    }
}

// validações:
// exibir mensagem de erro caso não seja possível consumir o web service de origem
// verificar se há mais de um pedido 
// verificar cpf e cnpj
// perguntar:
// como acessar apenas uma classe do web service
// como colocar uma variável no link e como ir mudando ela